﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SkypeInterviews {
	public class SkypeParameters {
		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "Fecha de asesoria")]
		public DateTime DateTime { get; set; }

		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "Duración asesoria")]
		public TimeSpan Duration { get; set; }

		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "Descripción")]
		[StringLength(500, ErrorMessage = "El campo {0} debe tener como mínimo {2} y máximo {1} caracteres de longitud.", MinimumLength = 50)]
		public string Description { get; set; }
	}
}