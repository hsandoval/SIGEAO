﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace SkypeInterviews {
		public interface IApiSkype {
				string ApiKey { get; set; }
				SkypeParameters Parametros { get; set; }
		}


		public class ApiSkype : Controller, IApiSkype {
				const string BASEURL = "http://interviews.skype.com";
				const string URI = "/api/interviews";
				public string APIKEY { get; set; }

				public string ApiKey { get; set; }
				public SkypeParameters Parametros { get; set; }

				public ApiSkype(string ApiKey, SkypeParameters Parameters) {
						APIKEY = ApiKey ?? null;
						Parameters = Parameters ?? null;
				}

				public ActionResult CreateAppointment() {
						return Json(new {
								Response = RequestSkype()
						}, JsonRequestBehavior.AllowGet);
				}

				public string RequestSkype() {
						using (var client = new HttpClient()) {
								client.BaseAddress = new Uri(BASEURL);
								client.DefaultRequestHeaders.Add("skype-apikey", APIKEY);

								var content = new FormUrlEncodedContent(new[] {
					new KeyValuePair<string, string>("", "")
				});

								var result = client.PostAsync(URI, content).Result;
								string resultContent = result.Content.ReadAsStringAsync().Result;
								return resultContent;
						}
				}
		}
}
