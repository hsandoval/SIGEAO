﻿using System.Web;
using System.Web.Optimization;

namespace SIGEAO {
		public class BundleConfig {
				// For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
				public static void RegisterBundles(BundleCollection bundles) {

						#region Styles

						bundles.Add(new StyleBundle("~/BootstrapCSS").
								Include(
										"~/Content/Styles/bootstrap/bootstrap.min.css",
										"~/Content/Styles/bootstrap/font-awesome.css"
								)
						);

						bundles.Add(new StyleBundle("~/Jasny")
								.Include(
										"~/Content/Styles/plugins/jasny/jasny-bootstrap.min.css"
								)
						);

						bundles.Add(new StyleBundle("~/AwesomeBootstrapCheckbox")
								.Include(
										"~/Content/Styles/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
								)
						);

						bundles.Add(new StyleBundle("~/Touchspin")
								.Include(
										"~/Content/Styles/plugins/touchspin/jquery.bootstrap-touchspin.min.css"
								)
						);

						bundles.Add(new StyleBundle("~/ICheck")
								.Include(
										"~/Content/Styles/plugins/iCheck/custom.css"
								)
						);

						bundles.Add(new StyleBundle("~/InspiniaCSS")
								.Include(
										"~/Content/Styles/dist/animate.css",
										"~/Content/Styles/dist/style.css"
								)
						);

						// bootstrap-datetimepicker
						bundles.Add(new StyleBundle("~/DateTimePickerCSS").Include(
										"~/Content/Styles/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"
								)
						);

						#endregion

						#region Scripts

						bundles.Add(new ScriptBundle("~/Modernizr")
								.Include(
										"~/Content/Scripts/modernizr/modernizr-*"
								)
						);

						bundles.Add(new ScriptBundle("~/BootstrapJS")
								.Include(
										"~/Content/Scripts/bootstrap/bootstrap.js",
										"~/Content/Scripts/bootstrap/respond.js",
										"~/Content/Scripts/bootstrap/bootbox.min.js"
								)
						);

						bundles.Add(new ScriptBundle("~/JQuery")
								.Include(
										"~/Content/Scripts/jquery/jquery-{version}.js"
								)
						);

						bundles.Add(new ScriptBundle("~/MetisMenu")
								.Include(
										"~/Content/Scripts/plugins/metisMenu/jquery.metisMenu.js"
								)
						);

						bundles.Add(new ScriptBundle("~/SlimScroll")
								.Include(
										"~/Content/Scripts/plugins/slimscroll/jquery.slimscroll.min.js"
								)
						);

						bundles.Add(new ScriptBundle("~/Pace")
								.Include(
										"~/Content/Scripts/plugins/pace/pace.min.js"
								)
						);

						bundles.Add(new ScriptBundle("~/InspiniaJS")
								.Include(
										"~/Content/Scripts/inspinia/inspinia.js"
								)
						);

						bundles.Add(new ScriptBundle("~/DateTimePickerJS")
								.Include(
										"~/Content/Scripts/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"
								)
						);

						bundles.Add(new ScriptBundle("~/MomentJS")
								.Include(
										"~/Content/Scripts/plugins/moment/moment.min.js",
										"~/Content/Scripts/plugins/moment/locales.min.js"
								)
						);

						#endregion
				}
		}
}
