﻿using SIGEAO.Models.Bussiness;

namespace SIGEAO.Models.ViewModels {
	public class AppointmentViewModel : IViewModel {
		public string TitleView { get; set; }
		public SkypeParameters SkypeParameters { get; set; }

		public AppointmentViewModel() {
			TitleView = "Citas";
		}
	}
}