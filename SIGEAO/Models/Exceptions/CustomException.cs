﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIGEAO.Models.Exceptions {
		public class CustomException : Exception {

				public List<string> ErrorMessage { get; set; }
				public Exception Exception { get; set; }
				public CustomException() { }
				public CustomException(string message) : base(message) { }
				public CustomException(string message, Exception inner) : base(message, inner) { }
				public CustomException(ModelStateDictionary modelState) {
						List<string> ErrorList = new List<string>();

						foreach (ModelState model in modelState.Values) {
								foreach (ModelError error in model.Errors) {
										if (error.ErrorMessage != null) {
												ErrorList.Add(error.ErrorMessage);
										}
								}
						}
						ErrorMessage = ErrorList;
						Exception = new Exception(String.Format("Ha ocurrido un error en el cumplimiento de validaciones"));
				}

		}
}