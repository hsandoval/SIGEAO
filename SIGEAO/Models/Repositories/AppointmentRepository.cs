﻿using SIGEAO.Models.ViewModels;
using System;
using SkypeInterviews;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;

namespace SIGEAO.Models.Repositories {
	public interface IAppointmentRepository {
		AppointmentViewModel TrySaveAppointment(AppointmentViewModel model);
	}

	public class AppointmentRepository : IAppointmentRepository {

		SIGEAO_Entities_EF EF = new SIGEAO_Entities_EF();

		public AppointmentViewModel TrySaveAppointment(AppointmentViewModel model) {
			try {
				SaveAppointmentAndGenerateAppointmentSkype(model);
				return new AppointmentViewModel();
			} catch (Exception) {

				throw;
			}
		}

		void SaveAppointmentAndGenerateAppointmentSkype(AppointmentViewModel model) {
			var response = GenerateAppointmentSkype(model);
			SaveAppointment(model);
		}

		void SaveAppointment(AppointmentViewModel model) {
			Appointment Appointment = new Appointment() {

			};
		}

		object GenerateAppointmentSkype(AppointmentViewModel model) {
			ApiSkype ApiSkype = new ApiSkype(WebConfigurationManager.AppSettings["ApiKeySkype"], ConvertParametersToParametersSkypeInterviews(model));
			return ApiSkype.CreateAppointment();
		}

		private SkypeParameters ConvertParametersToParametersSkypeInterviews(AppointmentViewModel model) => new SkypeParameters() {
			DateTime = model.SkypeParameters.DateTime,
			Duration = model.SkypeParameters.Duration,
			Description = model.SkypeParameters.Description
		};

		Appointment ConvertViewModelToModel(AppointmentViewModel model) => new Appointment() { };


	}
}