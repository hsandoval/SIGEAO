﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEAO.Models.Repositories {
  public interface IRepository {

	SIGEAO_Entities_EF _EF { get; set; }

  }

  public class Repository : IRepository {

	public SIGEAO_Entities_EF _EF { get; set; }

	public Repository() {
	  _EF = new SIGEAO_Entities_EF();
	}
  }
}
