﻿using SIGEAO.Models.Repositories;
using SIGEAO.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIGEAO.Controllers {
	public class AppointmentController : Controller {

		public IAppointmentRepository repository = new AppointmentRepository();

		public ActionResult Index() {
			return View("Index", new AppointmentViewModel());
		}

		[HttpPost]
		public ActionResult Create(AppointmentViewModel model) {
			try {
				repository.TrySaveAppointment(model);
				return RedirectToAction("Index");
			} catch (Exception ex) {
				ModelState.AddModelError("", "");
				return View(model);
			}
		}
	}
}
