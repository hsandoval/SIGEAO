﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SIGEAO.Startup))]
namespace SIGEAO
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
