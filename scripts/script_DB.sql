USE [SIGEAO]
GO

/****** Object: Table [dbo].[Appointments] Script Date: 13/03/2018 23:58:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('Appointments','U') IS NOT NULL
    DROP TABLE Appointments
    GO

IF OBJECT_ID('Types','U') IS NOT NULL
    DROP TABLE Types
    GO

IF OBJECT_ID('Categories','U') IS NOT NULL
    DROP TABLE Categories
    GO



CREATE TABLE dbo.Appointments (
    AppointmentId	INT				NOT NULL IDENTITY (1, 1),
    PathURL			VARCHAR (50)	NOT NULL,
    Enabled			BIT				NOT NULL
		CONSTRAINT Df_Appointments_Enabled DEFAULT 1,
	Deleted			BIT				NOT NULL
		CONSTRAINT Df_Appointments_Deleted DEFAULT 0
    CONSTRAINT  Pk_Appointments
        PRIMARY KEY (AppointmentId)
)
GO

CREATE TABLE dbo.Categories (
	CategoryId		INT				NOT NULL IDENTITY(1, 1),
	Description		VARCHAR(50)		NOT NULL,
    Enabled			BIT				NOT NULL
		CONSTRAINT Df_Categories_Enabled DEFAULT 1,
	Deleted			BIT				NOT NULL
		CONSTRAINT Df_Categories_Deleted DEFAULT 0
    CONSTRAINT Pk_Categories
        PRIMARY KEY (CategoryId),
	CONSTRAINT Uk_Categories
		UNIQUE (Description)
)
GO

CREATE TABLE dbo.Types (
	TypeId			INT				NOT NULL IDENTITY(1, 1),
	Description		VARCHAR(50)		NOT NULL,
	Fa_Bootstrap		VARCHAR(50)		NULL,
    Enabled			BIT				NOT NULL
		CONSTRAINT Df_Types_Enabled DEFAULT 1,
	Deleted			BIT				NOT NULL
		CONSTRAINT Df_Types_Deleted DEFAULT 0,
	CategoryFk		INT				NOT NULL,
	TypeFk			INT				NULL	
    CONSTRAINT Pk_Types
        PRIMARY KEY (TypeId),
	CONSTRAINT Uk_Types
		UNIQUE (Description, TypeFk),
	CONSTRAINT Fk_Types_Ref_Categories
		FOREIGN KEY (TypeId)
		REFERENCES Categories(CategoryId),
	CONSTRAINT FK_Types_Ref_Types
		FOREIGN KEY (TypeFk)
		REFERENCES Types(TypeId)
)
Go