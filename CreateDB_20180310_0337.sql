﻿CREATE TABLE Appointments (
	AppointmentId		INT				NOT NULL IDENTITY(1,1),
	PathURL				VARCHAR(50)		NOT NULL,
	Enabled				BIT				NOT NULL
	CONSTRAINT Pk_Appoinment
		PRIMARY KEY (AppointmentId)
)
GO

ALTER TABLE Appointments
ADD 
	CONSTRAINT Df_Appointment_Enabled 
		DEFAULT 1 FOR [Enabled]
GO
